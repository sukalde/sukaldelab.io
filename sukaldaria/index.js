var yaml = require('js-yaml');
var fs = require('fs-extra');
var path = require('path');
var pug = require('pug');
var Promise = require("bluebird");
var _ = require('lodash');

var OSAGAIAK_DIR = path.resolve(__dirname + '/../osagaiak');
var ERREZETAK_DIR = path.resolve(__dirname + '/../errezetak');
var TRESNAK_DIR = path.resolve(__dirname + '/../tresnak');

var osagaiak = {};
var errezetak = {};
var tresnak = {};

function osagaiakProzesatu() {
  return new Promise(function (resolve, reject) {
    var osagaiKopurua = 0;
    fs.walk(OSAGAIAK_DIR)
    .on('data', function (item) {
      if (!item.stats.isDirectory()) {
        var key = item.path.replace(OSAGAIAK_DIR + '/', '').replace('.yml','');
        var keyArray = key.split('/');
        var a = keyArray.pop(),
            b = keyArray.pop();
        if(a === b){
          keyArray.push(a);
          try {
            var doc = yaml.safeLoad(fs.readFileSync(item.path, 'utf8'));
            doc.errezetak = {};
            if(doc.garaia) {
              var d = new Date();
              var m = d.getMonth() + 1;
              if(doc.garaia[m]){
                doc.garaikoa = true;
              } else {
                doc.garaikoa = false;
              }
            } else {
              doc.garaikoa = true;
              doc.urteosokoa = true;
            }
            _.set(osagaiak, keyArray, doc);
            osagaiKopurua++;
          } catch (e) {
            console.log(e);
          }
        }
      }
    })
    .on('end', function () {
      console.log(osagaiKopurua + ' osagai prozesatu dira.');
      resolve();
    });
  });
}

function tresnakProzesatu() {
  return new Promise(function (resolve, reject) {
    fs.walk(TRESNAK_DIR)
    .on('data', function (item) {
      if (!item.stats.isDirectory()) {
        var key = item.path.replace(TRESNAK_DIR + '/', '').replace('.yml','');
        var keyArray = key.split('/');
        var a = keyArray.pop(),
            b = keyArray.pop();
        if(a === b){
          keyArray.push(a);
          key = keyArray.join('/');
          try {
            var doc = yaml.safeLoad(fs.readFileSync(item.path, 'utf8'));
            tresnak[key] = doc;
          } catch (e) {
            console.log(e);
          }
        }
      }
    })
    .on('end', function () {
      console.log(Object.keys(tresnak).length + ' tresna prozesatu dira.');
      resolve();
    });
  });
}

function Estekalaria(testua){
  return function estekalaria(gauza, pos){
    var esteka;
    if(gauza.indexOf('tresnak/') !== -1 || gauza.indexOf('teknikak/') !== -1) {
      esteka = '<a>' + gauza.replace('tresnak/','').replace('teknikak/','') + '</a>';
    } else {
      gauza = gauza.split('-');
      var izena = _.get(osagaiak, gauza[0].split('/')).izena;
      if(pos === 0) {
        izena = _.capitalize(izena);
      } else {

      }
      if(gauza[1]){
        izena = izena + gauza[1];
      }
      esteka = '<a href="/osagaiak/'+ gauza[0] +'/">' + izena  + '</a>';
    }

    return esteka;
  };
}



function errezetakProzesatu(){
  return new Promise(function (resolve, reject) {
    var errezetaKopurua = 0;
    fs.walk(ERREZETAK_DIR)
    .on('data', function (item) {
      if (!item.stats.isDirectory() && item.path.indexOf('.yml') !==-1) {
        try {
          var doc = yaml.safeLoad(fs.readFileSync(item.path, 'utf8'));
          var errezetaKey = item.path.replace(ERREZETAK_DIR + '/', '').replace('.yml','');
          var keyArray = errezetaKey.split('/');
          var a = keyArray.pop(),
              b = keyArray.pop();
          if(a === b){
            keyArray.push(a);
            for (var i = 0; i < doc.jarraibideak.length; i++) {
              if(_.isString(doc.jarraibideak[i])) {
                doc.jarraibideak[i] = doc.jarraibideak[i].replace(/\S+\/\S+/g, new Estekalaria(doc.jarraibideak[i]));
              } else {
                doc.jarraibideak[i].azalpena = doc.jarraibideak[i].azalpena.replace(/\S+\/\S+/g, new Estekalaria(doc.jarraibideak[i].azalpena));
              }
            }
            doc.garaikoa = true;
            _.forEach(doc.osagaiak, function(val, key, collection){
              var osagaia = {};
              if(_.isString(val)) {
                osagaia.kopurua = val;
              } else {
                if(_.isObject(val)){
                  osagaia = val;
                }
              }
              osagaia.info = _.get(osagaiak, key.split('/'));
              if(!osagaia.info.garaikoa) {
                doc.garaikoa = false;
              }
              _.set(osagaia.info.errezetak, keyArray, doc);
              collection[key] = osagaia;
            });
            _.set(errezetak, keyArray, doc);
            errezetaKopurua++;
          }

        } catch (e) {
          console.log(e);
        }
      }
    })
    .on('end', function () {
      console.log(errezetaKopurua + ' errezeta prozesatu dira');
      resolve();
    });
  });
}

function indizeaSortu(){
  var data = {
    osagaiak: osagaiak,
    errezetak: errezetak
  };
  var html = pug.renderFile(__dirname + '/txantiloiak/indizea.pug', data);
  fs.outputFileSync(__dirname + '/../public/index.html', html);
}

function errezetakRenderizatu(){
  return new Promise(function (resolve, reject) {
    var indizeak = {
      __lehena: []
    };
    function loop(kolekzioa, lastpath){
      _.forEach(kolekzioa, function(val, key){
        var path = key;
        if(lastpath) {
          path = lastpath + '/' + key;
          indizeak[lastpath].push(key);
        }
        if(val.izena) {
          var fitxategiIzena = __dirname + '/../public/errezetak/' + path + '/index.html';
          var html = pug.renderFile(__dirname + '/txantiloiak/errezeta.pug', val);
          fs.outputFileSync(fitxategiIzena, html);
        } else {
          indizeak[path] = [];
          indizeak.__lehena.push(path);
          loop(val, path);
        }
      });
    }
    loop(errezetak);
    _.forEach(indizeak, function(val, key){
      if(key === '__lehena') {
        var fitxategiIzena = __dirname + '/../public/errezetak/index.html';
        var html = pug.renderFile(__dirname + '/txantiloiak/errezeta_indizea.pug', { saila: "errezetak", errezetak: val});
        fs.outputFileSync(fitxategiIzena, html);
      } else {
        var fitxategiIzena = __dirname + '/../public/errezetak/' + key + '/index.html';
        var html = pug.renderFile(__dirname + '/txantiloiak/errezeta_indizea.pug', { saila: key, errezetak: val});
        fs.outputFileSync(fitxategiIzena, html);
      }
    });
    resolve();
  });
}



function osagaiakRenderizatu(){
  return new Promise(function (resolve, reject) {
    var indizeak = {
      __lehena: []
    };
    function loop(kolekzioa, lastpath){
      _.forEach(kolekzioa, function(val, key){
        var path = key;
        if(lastpath) {
          path = lastpath + '/' + key;
          // indizeak[lastpath].push(key);
        }
        if(val.izena) {
          var fitxategiIzena = __dirname + '/../public/osagaiak/' + path + '/index.html';
          var html = pug.renderFile(__dirname + '/txantiloiak/osagaia.pug', val);
          if(lastpath){
            val.key = key;
            indizeak[lastpath].push(val);
          }
          fs.outputFileSync(fitxategiIzena, html);
          _.forEach(val.irudiak, function(irudia){
            var jatorria = __dirname + '/../osagaiak/' + path + '/irudiak/' + irudia.fitxategia;
            var helburua = __dirname + '/../public/osagaiak/' + path + '/irudiak/' + irudia.fitxategia;
            fs.copySync(jatorria, helburua);
          });
        } else {
          indizeak[path] = [];
          indizeak.__lehena.push(path);
          loop(val, path);
        }
      });
    }
    loop(osagaiak);
    _.forEach(indizeak, function(val, key){
      if(key === '__lehena') {
        var fitxategiIzena = __dirname + '/../public/osagaiak/index.html';
        var html = pug.renderFile(__dirname + '/txantiloiak/osagai_indizea.pug', { saila: "osagaiak", osagaiak: val});
        fs.outputFileSync(fitxategiIzena, html);
      } else {
        var fitxategiIzena = __dirname + '/../public/osagaiak/' + key + '/index.html';
        var html = pug.renderFile(__dirname + '/txantiloiak/osagai_indizea.pug', { saila: key, osagaiak: val});
        fs.outputFileSync(fitxategiIzena, html);
      }
    });
    resolve();
  });
}

function tresnakRenderizatu(){
  return new Promise(function (resolve, reject) {
    var indizea = [];
    for (var tresna in tresnak) {
      indizea.push(tresna);
      var fitxategiIzena = __dirname + '/../public/tresnak/' + tresna + '/index.html';
      var html = pug.renderFile(__dirname + '/txantiloiak/tresna.pug', tresnak[tresna]);
      fs.outputFileSync(fitxategiIzena, html);
    }
    var fitxategiIzena = __dirname + '/../public/tresnak/index.html';
    var html = pug.renderFile(__dirname + '/txantiloiak/tresna_indizea.pug', { tresnak: indizea});
    fs.outputFileSync(fitxategiIzena, html);
    resolve();
  });
}

function fitxategiakPrestatu(){
  return new Promise(function (resolve, reject) {
    fs.removeSync(__dirname + '/../public');
    fs.ensureDirSync(__dirname + '/../public');
    fs.copySync(__dirname + '/../node_modules/bulma/css/bulma.css', __dirname + '/../public/css/bulma.css');
    resolve();
  });
}

function hasi(){
  fitxategiakPrestatu()
  .then(osagaiakProzesatu)
  .then(tresnakProzesatu)
  .then(errezetakProzesatu)
  .then(indizeaSortu)
  .then(osagaiakRenderizatu)
  .then(tresnakRenderizatu)
  .then(errezetakRenderizatu);
}

hasi();
